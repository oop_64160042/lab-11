package com.chanisata;

public class App {
    public static void main(String[] args) {
        System.out.println("-------Bird-------");
        Bird birt1 = new Bird("Jaokhunthong");
        Bird birt2 = new Bird("tweety");
        birt1.eat();
        birt1.sleep();
        birt1.takeoff();
        birt1.fly();
        birt1.landing();
        birt2.eat();
        birt2.sleep();
        birt2.takeoff();
        birt2.fly();
        birt2.landing();

        System.out.println("-------Plane-------");
        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        boeing.running();

        System.out.println("-------Superman-------");
        Superman clark = new Superman("Clark Kent");
        clark.eat();
        clark.sleep();
        clark.walk();
        clark.run();
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.swim();

        System.out.println("-------Human-------");
        Human human1 = new Human("Hinata Shoyo");
        human1.swim();
        human1.eat();
        human1.sleep();
        human1.walk();
        human1.run();

        System.out.println("-------Bat-------");
        Bat bat = new Bat("Bat Bat");
        bat.eat();
        bat.sleep();
        bat.takeoff();
        bat.fly();
        bat.landing();

        System.out.println("-------Snake-------");
        Snake snake = new Snake("Khamkeaw");
        snake.crawl();
        snake.eat();
        snake.sleep();

        System.out.println("-------Rat-------");
        Rat rat = new Rat("Jerry");
        rat.eat();
        rat.sleep();
        rat.walk();
        rat.run();

        System.out.println("-------Cat-------");
        Cat cat = new Cat("Tom");
        cat.sleep();
        cat.run();
        cat.walk();
        cat.eat();

        System.out.println("-------Dog-------");
        Dog dog = new Dog("Snoopy");
        dog.eat();
        dog.run();
        dog.walk();
        dog.sleep();

        System.out.println("-------Crocodile-------");
        Crocodile croc = new Crocodile("Chalawan");
        croc.swim();
        croc.crawl();
        croc.eat();
        croc.sleep();

        System.out.println("-------Fish-------");
        Fish buu = new Fish("Plabuuthong");
        buu.swim();
        buu.eat();
        buu.sleep();

        System.out.println("-------Bus-------");
        Bus bus = new Bus("Nakhonchaiair", "-");
        bus.running();

        System.out.println("-------Submarine-------");
        Submarine Subma = new Submarine("Payut K.", "-");
        Subma.running();
        Subma.swim();

        System.out.println();
        System.out.println("-------Flyable-------");
        Flyable[] flyables = { birt1, birt2, clark, boeing, bat };
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        System.out.println();
        System.out.println("-------Walkable-------");
        Walkable[] walkables = { clark, human1, rat, cat, dog };
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }

        System.out.println();
        System.out.println("-------Swimable-------");
        Swimable[] swimables = { clark, human1, croc, buu, Subma };
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }

        System.out.println();
        System.out.println("-------Swimable-------");
        Crawlable[] crawlables = { snake, croc };
        for (int i = 0; i < crawlables.length; i++) {
            crawlables[i].crawl();
        }

        System.out.println();
        System.out.println("-------Engin-------");
        Engine[] engines = { boeing,bus,Subma};
        for (int i = 0; i < engines.length; i++) {
            engines[i].running();
        }
    }
}
