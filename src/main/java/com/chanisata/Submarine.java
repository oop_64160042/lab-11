package com.chanisata;

public class Submarine extends Vehicle implements Engine, Swimable {
    
    public Submarine(String name, String engine) {
        super(name, engine);
    }

    @Override
    public String toString() {
        return "Submarine(" + getName() + ")";
    }

    @Override
    public void running() {
        System.out.println(this + " engine running. ");
        
    }

    @Override
    public void swim() {
        System.out.println(this + " swim. ");
    }
}
