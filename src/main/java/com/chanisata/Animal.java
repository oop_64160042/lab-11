package com.chanisata;

public abstract class Animal {
    private String name;
    private int numOfLeg;

    public Animal(String name, int numOfLeg) {
        this.name = name;
        this.numOfLeg = numOfLeg;
    }

    public String getName() {
        return this.name;
    }

    public int getNumOfLeg() {
        return this.numOfLeg;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumOfLeg(int numOfLeg) {
        this.numOfLeg = numOfLeg;
    }

    @Override
    public String toString() {
        return "Animal(" + name + ")";
    }

    public abstract void eat();
    public abstract void sleep();
}
