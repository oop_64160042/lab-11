package com.chanisata;

public class Fish extends Animal implements Swimable {

    public Fish(String name) {
        super(name, 0);
    }

    @Override
    public String toString() {
        return "Fish(" + this.getName() + ")";
    }

    @Override
    public void swim() {
        System.out.println(this + " swim. ");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat. ");
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep. ");
    }
}
