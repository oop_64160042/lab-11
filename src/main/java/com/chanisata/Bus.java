package com.chanisata;

public class Bus extends Vehicle implements Engine {

    public Bus(String name, String engine) {
        super(name, engine);
    }

    @Override
    public String toString() {
        return "Bus(" + getName() + ")";
    }

    @Override
    public void running() {
        System.out.println(this + " engine running. ");
        
    }
    
}
