package com.chanisata;

public interface Walkable {
    public void walk();
    public void run();
}
