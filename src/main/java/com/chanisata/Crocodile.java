package com.chanisata;

public class Crocodile extends Animal implements Crawlable, Swimable {

    public Crocodile(String name) {
        super(name, 4);
    }

    @Override
    public String toString() {
        return "Crocodile(" + this.getName() + ")";
    }

    @Override
    public void swim() {
        System.out.println(this + " swim. ");
    }

    @Override
    public void crawl() {
        System.out.println(this + " crawl. ");

    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat. ");
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep. ");
    }

}
